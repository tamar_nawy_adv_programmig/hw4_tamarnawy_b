#pragma once

#include <cstdio>
#include <iostream>
#include "osrng.h"
#include "modes.h"
#include <string.h>
#include <cstdlib>

//new
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#include <cstddef>
#include <hex.h>


#define NUM_OF_TRYES 5

using namespace std;


class CryptoDevice
{

public:
	CryptoDevice();
	~CryptoDevice();
	string encryptAES(string);
	string decryptAES(string);

	bool convertToHash(string password);

	bool logInFunc();

private:
	string _hashPassword = "CD60AF0EA80EC09C21AA8FCCB9E4DCF4";

};
