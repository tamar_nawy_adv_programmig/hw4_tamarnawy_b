#include "CryptoDevice.h"




//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];
byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];


CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}


/*
This function will encrypt the masseges of the clients with AES
INPUT: the massege to encrypt
OUTPUT: the encrypted massege
*/
string CryptoDevice::encryptAES(string plainText)
{

	string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

/*
This function will decrypt the masseges of the clients with AES
INPUT: the massege to decrypt
OUTPUT: the decrypted massege
*/
string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();
	

	return decryptedText;
}


/*
This function will encrypt the masseges of the clients with hash
INPUT: the massege to encrypt
OUTPUT: true if the password is good, false if not
*/
bool CryptoDevice::convertToHash(string password)
{

	string message = password;
	string output;
	bool returnVal = false;

	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

	CryptoPP::HexEncoder encoder;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();

	std::cout << output << std::endl;
	std::cout << _hashPassword << std::endl;

	if (_hashPassword.compare(output) == 0) //if the password that the user write eqwal to the one that save
	{
		returnVal = true;
	}
	else //if the password that the user write not eqwal to the one that save
	{
		returnVal = false;
	}
	return returnVal;
}

/*
This function will get from user the userName and the password
INPUT: nun
OUTPUT: true if the password is good, false if not
*/
bool CryptoDevice::logInFunc()
{
	string name;
	string password;

	
	cout << "name: ";
	cin >> name;

	cout << "password: ";
	cin >> password;

	bool good = convertToHash(password);

	

	return good;
}


